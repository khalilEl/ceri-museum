package com.example.cerimuseum;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

//Adapter les images de l'item (pictures) au view pager //
public class ImageAdapter extends PagerAdapter {
    private Context mContext;
    private List<Bitmap> images = new ArrayList<>();
    private List<String> Names ;
    private TextView txt_name;

   // private int[] mImageIds = new int[]{R.drawable.bee, R.drawable.cello, R.drawable.lion, R.drawable.pumpkin, R.drawable.watch};
    ImageAdapter(Context context,List<Bitmap> images,List<String> Names,TextView txt_name) {
        this.images = images;
        this.Names = Names;
        this.txt_name = txt_name;
        mContext = context;
    }
    @Override
    public int getCount() {
        //return mImageIds.length;
        return images.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(images.get(position));

        //imageView.setImageResource(mImageIds[position]);
        container.addView(imageView, 0);
        return imageView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

}