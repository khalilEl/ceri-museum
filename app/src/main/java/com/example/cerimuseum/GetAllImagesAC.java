package com.example.cerimuseum;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.viewpager.widget.ViewPager;

//Trouver toutes les images de l'item
class GetAllImagesAC extends AsyncTask<String, Void, List<Bitmap>> {

    String id;
    List<Bitmap> BitmapImages;
    Map<String,String> pictures;
    Context context;
    ViewPager viewPager;
    TextView txt_name;

    public GetAllImagesAC(ViewPager viewPager, Context context, String id, Map<String,String> pictures, List<Bitmap> BitmapImages, TextView txt_name) {
        this.viewPager = viewPager;
        this.context = context;
        this.id = id;
        this.txt_name = txt_name;
        this.BitmapImages = BitmapImages;
        this.pictures = pictures;
    }

    @Override
    protected List<Bitmap> doInBackground(String... strings) {


        for(String IMG : pictures.keySet()){
            Bitmap bmp = null;
            try {
                URL imageURL = WebServiceUrl.buildImages(id,IMG.trim());

                try {
                    if (imageURL != null) {
                        InputStream in = new java.net.URL(imageURL.toString()).openStream();
                        bmp = BitmapFactory.decodeStream(in);
                        BitmapImages.add(bmp);
                    }
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return BitmapImages;
    }

    @Override
    protected void onPostExecute(List<Bitmap> bitmaps) {
        super.onPostExecute(bitmaps);
        Log.i("GetAllImages", id+" - > "+BitmapImages.size());
        ImageAdapter adapter = new ImageAdapter(context,BitmapImages,new ArrayList<String>(pictures.values()),txt_name);
        viewPager.setAdapter(adapter);
        txt_name.setText(new ArrayList<String>(pictures.values()).get(0));
    }
}

