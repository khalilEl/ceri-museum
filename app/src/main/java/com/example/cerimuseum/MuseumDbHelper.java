package com.example.cerimuseum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class MuseumDbHelper extends SQLiteOpenHelper {

    private static final String TAG = MuseumDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 3;

    public static final String DATABASE_NAME = "cerimuseum.db";

    //public static final String TABLE_NAME = "cerimuseum";

   // public static final String COLUMN_MUSEUM_NAME = "Museum";

  //  public static final String DATABASE_NAME = "museum.db";

    public static final String TABLE_NAME = "catalog";

    public static final String _ID = "_id";
    public static final String COLUMN_WEB_ID = "web_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_THUMBNAIL = "thumbnail";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_TIME_FRAME = "time_frame";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_WORKING = "working";
    public static final String COLUMN_PICTURES = "pictures";
    public static final String COLUMN_TECHNICAL_DETAILS = "technical_details";
    public static final String COLUMN_LAST_UPDATE = "last_update";


    public MuseumDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_WEB_ID + " TEXT NOT NULL, " +
                COLUMN_NAME + " TEXT NOT NULL, " +
                COLUMN_THUMBNAIL + " BLOB, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_YEAR + " INTEGER, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_PICTURES + " TEXT, " +
                COLUMN_TECHNICAL_DETAILS + " INTEGER, " +
                COLUMN_LAST_UPDATE+ " TEXT, " +
                COLUMN_WORKING+ " TEXT, " +
                // To assure the application have just one item entry per
                // item name and brand, it's created a UNIQUE
                " UNIQUE (" + COLUMN_NAME + ", " +
                COLUMN_BRAND + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(newVersion != oldVersion) {
            db.execSQL("drop table if exists " + TABLE_NAME);
            onCreate(db);
        }

    }

    /**
     * Fills ContentValues result from an Item object
     */
    private ContentValues fill(MuseumObject item) {
        Gson gson = new Gson();

        ContentValues values = new ContentValues();
        values.put(COLUMN_WEB_ID, item.getId());
        values.put(COLUMN_NAME, item.getName());
        values.put(COLUMN_THUMBNAIL, item.getThumbnailAsbyte());
        values.put(COLUMN_BRAND, item.getBrand());
        values.put(COLUMN_YEAR, item.getYear());
        //values.put(COLUMN_TIME_FRAME, item.getTimeFrameString());
        values.put(COLUMN_CATEGORIES, item.getCategoriesString());
        values.put(COLUMN_DESCRIPTION, item.getDescription());
        values.put(COLUMN_TIME_FRAME, gson.toJson(Arrays.asList(item.getTimeFrame())));
        values.put(COLUMN_CATEGORIES, gson.toJson(item.getCategories()));
        //values.put(COLUMN_PICTURES, gson.toJson(item.getPictures()));

        values.put(COLUMN_PICTURES, item.getPicturesString());///
//        values.put(COLUMN_TECHNICAL_DETAILS, gson.toJson(Arrays.asList(item.getTechnicalDetails())));
       //values.put(COLUMN_LAST_UPDATE, item.getLastUpdate());
        return values;
    }

    /**
     * Adds a new item
     * @return  true if the item was added to the table ; false otherwise (case when the pair (name, brand) is
     * already in the data base)
     */
    public boolean addItem(MuseumObject item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(item);

        Log.d(TAG, "adding: "+ item.getName()+" with id="+ item.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (name, brand)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Returns a list on all the MuseumObjects of the data base
     */
    public List<MuseumObject> getAllTeams() {
        // TODO faite
        List<MuseumObject> res = new ArrayList<>();

        Cursor c = this.fetchAllMuseumObjects();
        while(c.moveToNext()) {
           res.add(this.cursorMuseumObject(c));
        }
        c.close();

        return res;
    }

    public MuseumObject cursorMuseumObject(Cursor cursor) {

        Gson gson = new Gson();
        ArrayList<Integer> timeFrames = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME)), ArrayList.class);
        int[] myArray = new int[timeFrames.size()];
        ArrayList<String> categories = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES)), ArrayList.class);
        String[] array = new String[categories.size()];
        categories.toArray(array);
        ArrayList<String> technicalDetails = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICAL_DETAILS)), ArrayList.class);
        String[] array2 = new String[categories.size()];
        categories.toArray(array2);
        //initialisation
        MuseumObject museumObject = new MuseumObject(
                cursor.getString(cursor.getColumnIndex(COLUMN_WEB_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                array,
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                myArray,
                cursor.getInt(cursor.getColumnIndex(COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                array2,
                Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(COLUMN_WORKING))),
                null

        );
        return museumObject;
    }


    public MuseumObject getItem(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                COLUMN_NAME + " = ? ", new String[]{name}, null, null, null, "1");
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        return this.cursorMuseumObject(cursor);
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows=adding"+numRows);
        db.close();
    }

    /**
     * Returns a cursor on all the teams of the data base
     */
    public Cursor fetchAllMuseumObjects() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllTeams()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


    public boolean addMuseumObject(MuseumObject museumObject) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = null;// = fill(museumObject);

        Log.d(TAG, "adding: "+museumObject.getName()+" with id="+museumObject.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public void deleteAllItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.close();
    }
}
