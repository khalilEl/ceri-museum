package com.example.cerimuseum;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//analyse syntaxiques des objets
public class JSONResponseHandlerMuseumObject {

    private static final String TAG = JSONResponseHandlerMuseumObject.class.getSimpleName();

    private MuseumObject museumObject;


    public JSONResponseHandlerMuseumObject(MuseumObject museumObject) {
        this.museumObject = museumObject;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMuseumObjectJson(reader);
        } finally {
            reader.close();
        }
    }



    private void readMuseumObjectJson(JsonReader reader) throws IOException {

          reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                Log.wtf(TAG,"name : "+name);
                //
                    if(name.equals("name")) {
                        String Name = reader.nextString();
                        Log.wtf(TAG,"Name : "+Name);
                        this.museumObject.setName(Name);

                    } else if (name.equals("timeFrame")){

                        reader.beginArray();
                        List<Integer> ListtimeFrame = new ArrayList<>();
                        while (reader.hasNext()) {
                            ListtimeFrame.add(reader.nextInt());
                        }
                        reader.endArray();

                        int[]timeFrame = new int[ListtimeFrame.size()];
                        for(int i=0; i<ListtimeFrame.size(); i++){
                            timeFrame[i] = ListtimeFrame.get(i).intValue();
                        }
                        this.museumObject.setTimeFrame(timeFrame);
                        Log.wtf(TAG,"ListtimeFrame.size() : "+ListtimeFrame.size());

                    }else if (name.equals("brand")) {
                        String brand = reader.nextString();
                        Log.wtf(TAG,"brand : "+brand);
                        this.museumObject.setBrand(brand);
                    }
                    else if (name.equals("working")) {
                        Boolean working = reader.nextBoolean();
                        Log.wtf(TAG,"working : "+working);
                    }
                    else if (name.equals("categories")) {
                        reader.beginArray();
                        List<String> Listcategories = new ArrayList<>();
                        while (reader.hasNext()) {
                            Listcategories.add(reader.nextString());
                        }
                        reader.endArray();

                        String[] categories = new String[Listcategories.size()];

                        for(int i=0; i<Listcategories.size(); i++){
                            categories[i] = Listcategories.get(i);
                        }

                        this.museumObject.setCategories(categories);

                        Log.wtf(TAG,"Listcategories.size() : "+Listcategories.size());
                    }
                    else if (name.equals("technicalDetails")) {
                        reader.beginArray();
                        List<String> ListtechnicalDetails= new ArrayList<>();
                        while (reader.hasNext()) {
                            ListtechnicalDetails.add(reader.nextString());
                        }
                        reader.endArray();

                        String[] technicalDetails = new String[ListtechnicalDetails.size()];

                        for(int i=0; i<ListtechnicalDetails.size(); i++){
                            technicalDetails[i] = ListtechnicalDetails.get(i);
                        }

                        this.museumObject.setTechnicalDetails(technicalDetails);

                        Log.wtf(TAG,"Listcategories.size() : "+ListtechnicalDetails.size());
                    }else if (name.equals("description")) {
                        String description = reader.nextString();
                        Log.wtf(TAG,"description "+description);
                        this.museumObject.setDescription(description);
                    }
                    else if (name.equals("year")) {
                        int year = reader.nextInt();
                        Log.wtf(TAG,"year : "+year);

                        this.museumObject.setYear(year);


                    }
                    else if (name.equals("pictures")) {
                        Map<String,String> pictures = new HashMap<String,String>();
                        reader.beginObject();
                        while (reader.hasNext()) {
                            //String nameReader = reader.nextName();
                            String nameReadernextName = reader.nextName();
                            String nameReaderString = reader.nextString();

                            pictures.put(nameReadernextName,nameReaderString);
                            //Log.wtf(TAG,"pictures key "+pictures.values().size());

                            Log.wtf(TAG,"nameReadernextName : "+nameReadernextName);
                            Log.wtf(TAG,"nameReaderString : "+nameReaderString);

                        }

                        reader.endObject();

                        this.museumObject.setPictures(pictures);

                    }
                    else {
                        reader.skipValue();
                    }

           }
            reader.endObject();

            String InId = this.museumObject.getId();
            //this.museumObject = new MuseumObject(InId,Name,categories,description,timeFrame,year,brand,technicalDetails,working,pictures);

        //}
       // reader.endArray();
    }

}
