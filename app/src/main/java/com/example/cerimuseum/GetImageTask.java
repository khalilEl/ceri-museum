package com.example.cerimuseum;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import androidx.recyclerview.widget.RecyclerView;

//Trouver le Thumbnail de l'item
public class GetImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    MuseumObject museumObject;
    RecyclerViewAdapter adapter;

    public GetImageTask(MuseumObject museumObject, ImageView bmImage) {
        this.museumObject = museumObject;
        this.bmImage = bmImage;
    }
    public GetImageTask(MuseumObject museumObject, RecyclerViewAdapter adapter) {
        this.museumObject = museumObject;
        this.adapter = adapter;
    }

    protected Bitmap doInBackground(String... urls) {
        Bitmap bmp = null;
        try {
            URL imageURL = WebServiceUrl.buildThumbnail(museumObject.getId());
            Log.i("image","Recherche de l'image "+museumObject.getId());
            try {
                if (imageURL != null) {
                    InputStream in = new java.net.URL(imageURL.toString()).openStream();
                    bmp = BitmapFactory.decodeStream(in);


                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return bmp;
    }
    protected void onPostExecute(Bitmap result) {
        if(bmImage != null)bmImage.setImageBitmap(result);
        museumObject.setThumbnail(result);
        if(adapter != null)adapter.notifyDataSetChanged();
        Log.i("image","l'image de "+museumObject.getId());

    }
}
