package com.example.cerimuseum;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;

//Class pricipale des objects
public class MuseumObject implements Serializable,Parcelable {

    public static final String TAG = MuseumObject.class.getSimpleName();

    public static transient Set<String> AllCategories = new HashSet<String>() ;

    private String id;
    private String name; // Nom de l’objet.
    private String[] categories; // Liste des catégories auxquelles appartient l’objet
    private String description; // Description de l’objet
    private int[] timeFrame; // Décennies durant lesquelles cet objet a été commercialisé ou utilisé

    private int year; // Année d’apparition de l’objet
    private String brand; // Marque de la société ayant produit l’objet
    private String[] technicalDetails; //Liste de caractéristiques techniques
    private Boolean working; //Indique si l’appareil est en état de fonctionner
    private Map<String,String> pictures; // Liste d’images liées à cet objet
    private Bitmap thumbnail;


    protected MuseumObject(Parcel in) {
        id = in.readString();
        name = in.readString();
        categories = in.createStringArray();
        description = in.readString();
        timeFrame = in.createIntArray();
        year = in.readInt();
        brand = in.readString();
        technicalDetails = in.createStringArray();
        byte tmpWorking = in.readByte();
        working = tmpWorking == 0 ? null : tmpWorking == 1;
        thumbnail = in.readParcelable(Bitmap.class.getClassLoader());
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeStringArray(categories);
        dest.writeString(description);
        dest.writeIntArray(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeStringArray(technicalDetails);
        dest.writeByte((byte) (working == null ? 0 : working ? 1 : 2));
        dest.writeParcelable(thumbnail, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MuseumObject> CREATOR = new Creator<MuseumObject>() {
        @Override
        public MuseumObject createFromParcel(Parcel in) {
            return new MuseumObject(in);
        }

        @Override
        public MuseumObject[] newArray(int size) {
            return new MuseumObject[size];
        }
    };

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public MuseumObject(String id) {
        this.id=id;
    }

    public MuseumObject(String id,String name, String[] categories, String description, int[] timeFrame, int year, String brand, String[] technicalDetails, Boolean working, Map<String, String> pictures) {
        this.id = id;
        this.name = name;
        //this.categories = categories;
        this.setCategories(categories);
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
    }

    public MuseumObject(String id,String name, String[] categories, String description, int[] timeFrame) {
        this.name = name;
        //this.categories = categories;
        this.setCategories(categories);
        this.description = description;
        this.timeFrame = timeFrame;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCategories() {
        return Arrays.asList(categories);
    }

    public void setCategoriesTlb(String[] categories){
        this.categories = categories;
    }

    public void freeCategories(){
        this.categories = new String[]{};
    }

    public void setCategories(String[] categories) {

        for(int i = 0; i < categories.length;i++){
            if(!AllCategories.contains(categories[i])){
                AllCategories.add(categories[i]);
            }
        }

        this.categories = categories;
    }

    public String getCategoryDisplay() {
        String categoryDisplay ="";

        for (int i = 0; i < categories.length; i++) {
            //categoryDisplay = categoryDisplay + "[ " + categories[i] + " ] ";
            categoryDisplay = categoryDisplay + " "+ categories[i] ;
        }

        return categoryDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int[] timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getYear() {
        if(year <= 0 )return 9999;
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public Map<String, String> getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures;
    }

    public String getTimeFrameString() {
        String alltimeFrame = "";
        for(int i = 0; i < timeFrame.length  ;i++){
            alltimeFrame = alltimeFrame + "," + timeFrame[i];
        }
        return  alltimeFrame;
    }

    public String getCategoriesString() {

        String categoryDisplay ="";

        for (int i = 0; i < categories.length; i++) {
            categoryDisplay = categoryDisplay + "," + categories[i];
        }

        return categoryDisplay;
    }

    public String getPicturesString() {

        String PictureDisplay ="";

        if(this.pictures != null) {
            for (String picture : this.pictures.keySet()) {
                PictureDisplay = PictureDisplay + "," + picture;
            }
        }
        return PictureDisplay;
    }

    public String getTechnicalDetailsString() {

        String TechnicalDetails ="";

        for (int i = 0 ; i < technicalDetails.length; i++) {
            TechnicalDetails = TechnicalDetails + "," + technicalDetails[i];
        }

        return TechnicalDetails;
    }


    public byte[] getThumbnailAsbyte() {
        if(this.getThumbnail() != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            this.getThumbnail().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        }

        return new byte[]{};
    }
}
