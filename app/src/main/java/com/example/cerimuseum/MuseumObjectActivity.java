package com.example.cerimuseum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.abs;


//Activity des objetcs
public class MuseumObjectActivity extends AppCompatActivity {

    private static final String TAG = MuseumObjectActivity.class.getSimpleName();

    List<Bitmap> BitmapImages = new ArrayList<>();
    public ViewPager viewPager;
    MuseumObject museumObject;
    ChipGroup CatChipsGroup,TechdetailsGroup,DecenniesGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum_object);


        if(getIntent().getSerializableExtra("MuseumObject") != null){
            this.museumObject = (MuseumObject) getIntent().getSerializableExtra("MuseumObject");
            this.museumObject.setPictures(RecyclerViewAdapter.pictures );
        }

        //Views

        CatChipsGroup = findViewById(R.id.CatChipsGroup);
        DecenniesGroup = findViewById(R.id.DecenniesGroup);
        TechdetailsGroup = findViewById(R.id.TechdetailsGroup);
        TextView txtview_Name = findViewById(R.id.txtview_Name);
        TextView txtview_description = findViewById(R.id.txtview_description);
        //TextView txtview_technicalDetails = findViewById(R.id.txtview_technicalDetails);
        TextView txtview_working = findViewById(R.id.txtview_working);
        //TextView txtview_timeFrame = findViewById(R.id.txtview_timeFrame);
        TextView txtview_year = findViewById(R.id.txtview_year);
        TextView txtview_brand = findViewById(R.id.txtview_brand);
        final TextView txtView_numpage = findViewById(R.id.txtView_numpage);
         LinearLayout Layoutdescription =  findViewById(R.id.Layoutdescription);
        TextView dec_txtView = findViewById(R.id.dec_txtView);

        //
        txtview_Name.setText(museumObject.getName());
        if(museumObject.getDescription() != null || !museumObject.getDescription().isEmpty()){
            Layoutdescription.setVisibility(View.VISIBLE);
          //  dec_txtView.setVisibility(View.VISIBLE);
        txtview_description.setText(museumObject.getDescription());
        }else{
           Layoutdescription.setVisibility(View.GONE);
         //   dec_txtView.setVisibility(View.GONE);
        }

        if( museumObject.getWorking() != null && museumObject.getWorking()){
            txtview_working.setText("Oui");
        }else{
            txtview_working.setText("Non");
        }
        if(museumObject.getYear() != 0 && museumObject.getYear() != 9999)txtview_year.setText(String.valueOf(museumObject.getYear()));
        txtview_brand.setText(museumObject.getBrand());
        //
        if( museumObject.getTechnicalDetails() != null){
            List<String> list2 = new ArrayList<>();
            for(String tech : museumObject.getTechnicalDetails()){
                list2.add(tech);
            }
            setChips(R.layout.chip_item3, TechdetailsGroup, list2);
         }
        //

        if( museumObject.getTimeFrame() != null){
            List<String> list1 = new ArrayList<>();
            for(int time : museumObject.getTimeFrame()){
                list1.add(""+time);
            }
            setChips(R.layout.chip_item2, DecenniesGroup, list1);
        }

        //L'ajoute des categories
        setChips(R.layout.chip_item,CatChipsGroup,museumObject.getCategories());

        //Afficher la pagination pour chaque image
        if(museumObject.getPictures() != null){
            txtView_numpage.setText(1+"/"+museumObject.getPictures().keySet().size());
        }else{
            txtView_numpage.setText("Aucume image disponnible");
        }

        final TextView txtview_imageTitle = findViewById(R.id.txtview_imageTitle);
       viewPager = findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                txtview_imageTitle.setText(new ArrayList<>(museumObject.getPictures().values()).get(position));
                if(museumObject.getPictures() != null){
                    int index = position+1;
                    txtView_numpage.setText(index+"/"+museumObject.getPictures().keySet().size());
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //(ViewPager viewPager, Context context, String id, Map<String,String> pictures, List<Bitmap> BitmapImages, TextView txt_name) {
        //
       if(this.museumObject.getPictures() != null) {
           //Recuperation des objets selon une tache en arriere plan
            new GetAllImagesAC(viewPager, getApplicationContext(), museumObject.getId(), museumObject.getPictures(), BitmapImages, txtview_imageTitle).execute();
        }

    }


    //L'ajoute dans un chip groupe a partie d'une list et un theme pour le chip
    public void setChips( int resource,ChipGroup chipgroup,List<String> categorys) {
        for (String category :categorys) {

            Chip mChip = (Chip) MuseumObjectActivity.this.getLayoutInflater().inflate(resource, null, false);
            mChip.setText(category);
            //int paddingDp = (int) TypedValue.applyDimension(
             //       TypedValue.COMPLEX_UNIT_DIP, 10,
             //       getResources().getDisplayMetrics()
            //);
            // liste des couleurs
            String colorArray[] = {
                    "#c62828","#AD1457","#6A1B9A","#4527A0","#283593",
                    "#1565C0","#0277BD","#00838F","#00897B","#43A047",
                    "#558B2F","#E91E63","#FFA726","#F9A825","#FF8F00",
                    "#EF6C00","#D84315","#0389ff"};
            int mcolor = abs(category.hashCode()%18);
            // Log.wtf("mcolor",""+salle.getNom_salle());
            // Log.wtf("mcolor",""+mcolor);
            String Parsecolor = colorArray[mcolor];
            Log.wtf("Parsecolor",""+Parsecolor);
            int color = Color.parseColor(Parsecolor);
            Log.wtf("color",""+color);
            //mChip.setChipBackgroundColor(ColorStateList.valueOf(ContextCompat.getColor(context,color)));
            mChip.setTextColor(ColorStateList.valueOf(Color.WHITE));
            mChip.setChipBackgroundColor(ColorStateList.valueOf(color));
            mChip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                }
            });
            chipgroup.addView(mChip);
        }
    }
}
