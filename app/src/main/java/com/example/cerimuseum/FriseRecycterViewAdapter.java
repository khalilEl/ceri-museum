package com.example.cerimuseum;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import static java.lang.Math.abs;

public class FriseRecycterViewAdapter extends RecyclerView.Adapter<FriseViewHolder>{

    private Context context;
    private List<MuseumObject> museumObjects;
    private List<TriElement> firstElements = new ArrayList<>();

    public FriseRecycterViewAdapter(Context context, List<MuseumObject> museumObjects) {
        this.context = context;
        this.museumObjects = museumObjects;
        SortByChno();
    }


    @NonNull
    @Override
    public FriseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.frise_item, parent, false);
        FriseViewHolder Friseholder = new FriseViewHolder(view);
        return Friseholder;
    }

    @Override
    public void onBindViewHolder(@NonNull FriseViewHolder Friseholder, int position) {
        final MuseumObject museumObject = this.museumObjects.get(position);
        Friseholder.txtView_Name.setText(museumObjects.get(position).getName());

        if (HideOrShowAlpha(this.museumObjects.get(position))){
            if(museumObjects.get(position).getYear() != 9999){
                Friseholder.txtView_decennie.setText(""+museumObjects.get(position).getYear());
                Friseholder.txtView_decennie.setVisibility(View.VISIBLE);

            }else{
                Friseholder.txtView_decennie.setText(""+museumObjects.get(position).getYear());
                Friseholder.txtView_decennie.setVisibility(View.INVISIBLE);
            }

            Friseholder.cardviewdecennie.setVisibility(View.VISIBLE);
        }else{
            Friseholder.cardviewdecennie.setVisibility(View.INVISIBLE);
        }

        Friseholder.Chip_group.removeAllViews();
        for (String category :museumObject.getCategories()) {

            Chip mChip = new Chip(context);
            mChip.setText(category);

            // liste des couleurs
            String colorArray[] = {
                    "#c62828","#AD1457","#6A1B9A","#4527A0","#283593",
                    "#1565C0","#0277BD","#00838F","#00897B","#43A047",
                    "#558B2F","#E91E63","#FFA726","#F9A825","#FF8F00",
                    "#EF6C00","#D84315","#0389ff"};

            //Le choix des couleurs aleatoire
            int mcolor = abs(category.hashCode()%18);
            // Log.wtf("mcolor",""+salle.getNom_salle());
            // Log.wtf("mcolor",""+mcolor);
            String Parsecolor = colorArray[mcolor];
            //Log.wtf("Parsecolor",""+Parsecolor);
            int color = Color.parseColor(Parsecolor);
            //Log.wtf("color",""+color);
            //mChip.setChipBackgroundColor(ColorStateList.valueOf(ContextCompat.getColor(context,color)));
            mChip.setTextColor(ColorStateList.valueOf(Color.WHITE));
            mChip.setChipBackgroundColor(ColorStateList.valueOf(color));

            mChip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                }
            });
            Friseholder.Chip_group.addView(mChip);
        }

    }

    Boolean HideOrShowAlpha(MuseumObject museumObject){

            for (TriElement ShowfirstElement : this.firstElements) {
                if (ShowfirstElement.getName().equals(museumObject.getName())) {
                    return true;
                }
            }


        return false;
    }

    //Trier par ordre chnologique
    void SortByChno(){
        if (this.museumObjects.size() > 0) {
            Collections.sort(museumObjects, new Comparator<MuseumObject>() {
                @Override
                public int compare(final MuseumObject object1, final MuseumObject object2) {
                    if (object1.getYear() == object2.getYear()) {
                        return 0;
                    } else if (object1.getYear() > object2.getYear()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        }

        this.firstElements.clear();
        if(museumObjects.size() > 0) {
            int oldYear = 0;
            for(MuseumObject museumObject : museumObjects){
                if(museumObject.getYear() > 0){
                    oldYear = museumObject.getYear();
                    break;
                }

            }


            int newYear;
            int count = 0;
            for (MuseumObject museumObject : this.museumObjects) {
                newYear = museumObject.getYear();
                if (!(newYear == oldYear)) {
                    count = 0;
                }
                if (count == 0) {
                    firstElements.add(new TriElement(museumObject.getName(), museumObject.getYear(), museumObject.getCategories()));
                    count++;
                }
                oldYear = newYear;
                Log.i("frise",oldYear+" _ "+newYear);
            }
        }
        Log.i("frise","CHNO firstElements = "+firstElements.size());

        // for(FirstElement firstElement : firstElements){
        // Log.i(TAG,"CHNO firstElements NAme : "+firstElement.getName());
        // Log.i(TAG,"CHNO firstElements Year : "+firstElement.getYear());
        //}
    }

    @Override
    public int getItemCount() {
        return this.museumObjects.size();
    }
}



class FriseViewHolder extends RecyclerView.ViewHolder {


    TextView txtView_decennie;
    TextView txtView_Name;
    ChipGroup Chip_group;
    CardView cardviewdecennie;


    FriseViewHolder(View row) {
        super(row);
        txtView_decennie = row.findViewById(R.id.txtView_decennie);
        txtView_Name = row.findViewById(R.id.txtView_Name);
        Chip_group = row.findViewById(R.id.Chip_group);
        cardviewdecennie = row.findViewById(R.id.cardviewdecennie);
    }



}
