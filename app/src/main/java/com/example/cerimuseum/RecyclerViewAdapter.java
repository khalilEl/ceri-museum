package com.example.cerimuseum;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static java.lang.Math.abs;


//adapteur pricipal des RecyclerView + tri
public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private static final String TAG = RecyclerViewAdapter.class.getSimpleName();
    public static Map<String,String> pictures;
    private Context context;
    public List<MuseumObject> museumObjects;
    public String TriOption;
    private List<TriElement> firstElements = new ArrayList<>();
    public RecyclerViewAdapter(Context context, List<MuseumObject> museumObjects,String TriOption) {
        this.context = context;
        this.museumObjects = museumObjects;
        this.TriOption = TriOption;
        Log.i(TAG,"Trioption = "+TriOption);
        if(TriOption.equals("Alpha")){
            SortByAlpha();
        }else if(TriOption.equals("CHNO")){
            SortByChno();
        }else if(TriOption.equals("CAT")){
            this.museumObjects = new ArrayList<>(SortByCatgorie()) ;
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.layout_museum_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final MuseumObject museumObject = this.museumObjects.get(position);
        if(museumObject.getBrand() != null){
            holder.txtview_Marque.setText(museumObject.getBrand());
        }else{
            holder.txtview_Marque.setVisibility(View.GONE);
        }

        holder.txtview_Name.setText(museumObject.getName());

        holder.SectionTitle.setText(SectionLetter(this.museumObjects.get(position)).toUpperCase());

        if(TriOption.equals("CHNO")) {
            if (HideOrShowAlpha(this.museumObjects.get(position)) && this.museumObjects.get(position).getYear() < 9999) {
                holder.SectionLayout.setVisibility(View.VISIBLE);
            } else {
                holder.SectionLayout.setVisibility(View.GONE);
            }
        }else if(TriOption.equals("Alpha")){
            if (HideOrShowAlpha(this.museumObjects.get(position))) {
                holder.SectionLayout.setVisibility(View.VISIBLE);
            } else {
                holder.SectionLayout.setVisibility(View.GONE);
            }
        }else if(TriOption.equals("CAT")){
            if (HideOrShowAlpha(this.museumObjects.get(position))) {
                holder.SectionLayout.setVisibility(View.VISIBLE);
            } else {
                holder.SectionLayout.setVisibility(View.GONE);
            }
        }
        else{
            holder.SectionLayout.setVisibility(View.GONE);
        }

        holder.ObjectCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, MuseumObjectActivity.class);
                myIntent.putExtra("MuseumObject",(Serializable) museumObjects.get(position));
                pictures = museumObjects.get(position).getPictures();
                if(museumObjects.get(position).getPictures() != null)Log.i(TAG,"pic size "+museumObjects.get(position).getPictures().keySet().size());
                context.startActivity(myIntent);
            }
        });
        //
        if(museumObject.getThumbnail() == null) {
            GetImageTask setImageBadgeByUrlTask = new GetImageTask(museumObject, holder.imgView_MuseumObject);
            setImageBadgeByUrlTask.execute();
        }

        else{
            holder.imgView_MuseumObject.setImageBitmap(museumObject.getThumbnail());
        }
        //holder.imgView_MuseumObject.setImageBitmap(museumObject.getThumbnail());
        // Categories
        //holder.linearlayout_categories.removeAllViews();

        holder.ItemCategories.removeAllViews();
        for (String category :museumObject.getCategories()) {

            Chip mChip = new Chip(context);
            mChip.setText(category);

            // liste des couleurs
            String colorArray[] = {
                    "#c62828","#AD1457","#6A1B9A","#4527A0","#283593",
                    "#1565C0","#0277BD","#00838F","#00897B","#43A047",
                    "#558B2F","#E91E63","#FFA726","#F9A825","#FF8F00",
                    "#EF6C00","#D84315","#0389ff"};

            //Le choix des couleurs aleatoire
            int mcolor = abs(category.hashCode()%18);
           // Log.wtf("mcolor",""+salle.getNom_salle());
           // Log.wtf("mcolor",""+mcolor);
            String Parsecolor = colorArray[mcolor];
            //Log.wtf("Parsecolor",""+Parsecolor);
            int color = Color.parseColor(Parsecolor);
           //Log.wtf("color",""+color);
            //mChip.setChipBackgroundColor(ColorStateList.valueOf(ContextCompat.getColor(context,color)));
            mChip.setTextColor(ColorStateList.valueOf(Color.WHITE));
            mChip.setChipBackgroundColor(ColorStateList.valueOf(color));

            mChip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                }
            });
            holder.ItemCategories.addView(mChip);
        }

        //setChips(R.layout.chip_item4,holder.ItemCategories,museumObject.getCategories());


        Log.i(TAG," museumObject.getName() "+museumObject.getName());
    }

    //retour le nom de la section selon de type de tri
    String SectionLetter(MuseumObject museumObject){
        if(this.TriOption.equals("Alpha")){
            return  ""+museumObject.getName().charAt(0);
        }
        if(this.TriOption.equals("CHNO")){
            return  ""+museumObject.getYear();
        }
        if(this.TriOption.equals("CAT")){
            return  ""+museumObject.getCategories().get(0);
        }
        return  "";
    }

    //permet de savoir s'il faut afficher le nom de la section au non
    Boolean HideOrShowAlpha(MuseumObject museumObject){
        if(this.TriOption.equals("Alpha") || this.TriOption.equals("CHNO")) {
            for (TriElement ShowfirstElement : this.firstElements) {
                if (ShowfirstElement.getName().equals(museumObject.getName())) {
                    return true;
                }
            }
        }else if(this.TriOption.equals("CAT")) {

            for (TriElement ShowfirstElement : this.firstElements) {
                if (ShowfirstElement.getName().equals(museumObject.getName())){
                        //&& ShowfirstElement.getCategories().equals(museumObject.getCategories())) {
                    return true;
                }
            }

        }

        return false;
    }

    @Override
    public int getItemCount() {

        return this.museumObjects.size();
    }


    //Trier par ordre Alphabetique
    void SortByAlpha(){
        if (this.museumObjects.size() > 0) {
            Collections.sort(museumObjects, new Comparator<MuseumObject>() {
                @Override
                public int compare(final MuseumObject object1, final MuseumObject object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }

        this.firstElements.clear();
        if(museumObjects.size() > 0) {
            String olgSwitcher = "" + this.museumObjects.get(0).getName().charAt(0);
            String newSwitcher;
            int count = 0;
            for (MuseumObject museumObject : this.museumObjects) {
                newSwitcher = "" + museumObject.getName().charAt(0);
                if (!newSwitcher.equals(olgSwitcher)) {
                    count = 0;
                }
                if (count == 0) {
                    firstElements.add(new TriElement(museumObject.getName(), museumObject.getYear(), museumObject.getCategories()));
                    count++;
                }
                olgSwitcher = newSwitcher;

            }
        }
        Log.i(TAG,"firstElements = "+firstElements.size());
    }


    //Trier par ordre chnologique
    void SortByChno(){
        if (this.museumObjects.size() > 0) {
            Collections.sort(museumObjects, new Comparator<MuseumObject>() {
                @Override
                public int compare(final MuseumObject object1, final MuseumObject object2) {
                    if (object1.getYear() == object2.getYear()) {
                        return 0;
                    } else if (object1.getYear() > object2.getYear()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        }

        this.firstElements.clear();
        if(museumObjects.size() > 0) {
            int oldYear = 0;
            for(MuseumObject museumObject : museumObjects){
                if(museumObject.getYear() > 0){
                    oldYear = museumObject.getYear();
                    break;
                }

            }


            int newYear;
            int count = 0;
            for (MuseumObject museumObject : this.museumObjects) {
                newYear = museumObject.getYear();
                if (!(newYear == oldYear)) {
                    count = 0;
                }
                if (count == 0) {
                    firstElements.add(new TriElement(museumObject.getName(), museumObject.getYear(), museumObject.getCategories()));
                    count++;
                }
                oldYear = newYear;
                Log.i(TAG,oldYear+" _ "+newYear);
            }
        }
        Log.i(TAG,"CHNO firstElements = "+firstElements.size());

       // for(FirstElement firstElement : firstElements){
           // Log.i(TAG,"CHNO firstElements NAme : "+firstElement.getName());
           // Log.i(TAG,"CHNO firstElements Year : "+firstElement.getYear());
        //}
    }


    //Trier par categorie
    List<MuseumObject> SortByCatgorie() {
       List<MuseumObject> SortedMuseumObjetcs = new ArrayList<>();
        //Set<MuseumObject> SortedMuseumObjetcs = new HashSet<>();

        List<String> tempCategories = new ArrayList<>(MuseumObject.AllCategories);
        Collections.sort(tempCategories);
        MuseumObject.AllCategories = new HashSet<>(tempCategories);

        //String id,String name, String[] categories, String description, int[] timeFrame, int year, String brand, String[] technicalDetails, Boolean working, Map<String, String> pictures
        for(String TargetCategorie : MuseumObject.AllCategories){
            for(MuseumObject museumObject : this.museumObjects){
                if(museumObject.getCategories().contains(TargetCategorie)){

                    String[] tlbcat = new String[1];
                    tlbcat[0] = TargetCategorie;

                    //museumObjectnew.freeCategories() ;

                    //museumObject.setCategoriesTlb(tlbcat);

                    SortedMuseumObjetcs.add(museumObject);
                }
            }
        }

        Collections.sort(SortedMuseumObjetcs, new Comparator<MuseumObject>() {
            @Override
            public int compare(final MuseumObject object1, final MuseumObject object2) {
                if(object1.getCategories().containsAll(object2.getCategories())) {
                    return 0;
                }else if(object1.getCategories().contains(object2.getCategories())){
                    return 1;
                }
                else {
                    return -1;
                }
            }
        });

        /*
        *
        * */
        this.firstElements.clear();
        if(SortedMuseumObjetcs.size() > 0) {


            List<String> olgSwitcher = SortedMuseumObjetcs.get(0).getCategories();
            List<String> newSwitcher;
            int count = 0;
            for (MuseumObject museumObject : SortedMuseumObjetcs) {
                newSwitcher =  museumObject.getCategories();
                if (!newSwitcher.containsAll(olgSwitcher)) {
                    count = 0;
                }
                if (count == 0) {
                    firstElements.add(new TriElement(museumObject.getName(), museumObject.getYear(), museumObject.getCategories()));
                    count++;
                }
                olgSwitcher = newSwitcher;

            }
        }

        Log.i(TAG,"CAT SortedMuseumObjetcs = "+SortedMuseumObjetcs.size());
        Log.i(TAG,"CAT firstElements = "+firstElements.size());

        // for(FirstElement firstElement : firstElements){
        // Log.i(TAG,"CHNO firstElements NAme : "+firstElement.getName());
        // Log.i(TAG,"CHNO firstElements Year : "+firstElement.getYear());
        //}
    //}
        return new ArrayList<>(SortedMuseumObjetcs);
    }


}
class ViewHolder extends RecyclerView.ViewHolder {

    ImageView imgView_MuseumObject;
    TextView txtview_Name;
    TextView txtview_Marque;
    LinearLayout linearlayout_categories;
    TextView SectionTitle;
    LinearLayout SectionLayout;
    CardView ObjectCardView;
    ChipGroup ItemCategories;


    ViewHolder(View row) {
        super(row);
        imgView_MuseumObject = row.findViewById(R.id.imgView_MuseumObject);
         txtview_Name = row.findViewById(R.id.txtview_Name);
         txtview_Marque = row.findViewById(R.id.txtview_Marque);
         linearlayout_categories = row.findViewById(R.id.linearlayout_categories);
        SectionTitle = row.findViewById(R.id.SectionTitle);
        SectionLayout = row.findViewById(R.id.SectionLayout);
        ObjectCardView = row.findViewById(R.id.ObjectCardView);
        ItemCategories = row.findViewById(R.id.ItemCategories);

    }



}

//Element de tri principal
class TriElement {

    String Name;
    int year;
    List<String> categories;

    TriElement(String Name,int year,List<String> categories){
        this.Name = Name;
        this.year = year;
        this.categories = categories;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}





