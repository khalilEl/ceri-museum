package com.example.cerimuseum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


//Activity pricipal
public class MainActivity extends AppCompatActivity {

    //
    MuseumDbHelper museumDbHelper = new MuseumDbHelper(this);
    private String TriOption = "NONE" ;
    SearchView searchView;
    CardView filterPiker;
    TextView txtview_filter;
    List<MuseumObject> SearchList = new ArrayList<>();

    //
    public List<MuseumObject> ListmuseumObjects = new ArrayList<MuseumObject>();
    private RecyclerView RecyclerViewTeams;
    public RecyclerViewAdapter adapter;
    public FriseRecycterViewAdapter adapterFrise;
    String[] ListOFOptions = {"Normal" ,"ordre alphabétique", "ordre chronologique", "ordre par catalogue","Frise chronologique"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //le choix des filter de recherche
        filterPiker = findViewById(R.id.filterPiker);
        txtview_filter = findViewById(R.id.txtview_filter);
        filterPiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(txtview_filter);
            }
        });

        searchView = findViewById(R.id.searchView);


        //Le choix des tries
        final Spinner FilterSpinner = findViewById(R.id.FilterSpinner);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,ListOFOptions);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        FilterSpinner.setAdapter(arrayAdapter);

        FilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                switch (FilterSpinner.getSelectedItem().toString()){
                    case "Normal" :
                        TriOption = "NONE";
                        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

                        MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

                        RecyclerViewTeams.setAdapter(MainActivity.this.adapter);

                        break;
                    case "ordre alphabétique":
                        TriOption = "Alpha";
                        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

                        MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

                        RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                        break;
                    case "ordre chronologique":
                        TriOption = "CHNO";
                        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

                        MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

                        RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                        break;
                    case"ordre par catalogue" :
                        TriOption = "CAT";
                        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

                        MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

                        RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                        break;
                    case"Frise chronologique" :

                        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));
                        MainActivity.this.adapterFrise = new FriseRecycterViewAdapter(MainActivity.this, new ArrayList<MuseumObject>(ListmuseumObjects)
                                );
                        RecyclerViewTeams.setAdapter(MainActivity.this.adapterFrise);

                        break;

                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }


        });

        if(ListmuseumObjects.isEmpty() || museumDbHelper.getAllTeams().isEmpty()){
            new MuseumObjetcsIdsTask().execute();

        }



        RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
        RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(this, 0));

        this.adapter = new RecyclerViewAdapter(this, ListmuseumObjects, TriOption);

        RecyclerViewTeams.setAdapter(this.adapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("query",String.valueOf(txtview_filter.getText()).trim());
                getSearchedMuseumObject(query.trim(), String.valueOf(txtview_filter.getText()).trim());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
                RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                return false;
            }
        });





        //try {

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);


            //Log.i("MAIN", String.valueOf(WebServiceUrl.buildGetMuseumObject("rxs")));

            //HttpURLConnection urlConnection = (HttpURLConnection) WebServiceUrl.buildGetMuseumObject("rxs").openConnection();
            // try {
                // InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                // JSONResponseHandlerMuseumObject jsonMuseumObject = new JSONResponseHandlerMuseumObject();
                // jsonMuseumObject.readJsonStream(in);
                // } finally {
               //  urlConnection.disconnect();
                // }


           // } catch (Exception e) {
             //   e.printStackTrace();
           // }

    }

    // Tache en Arriere Plan id item
    private class MuseumObjetcsIdsTask extends AsyncTask<Void, Integer, List<String>> {

        List<String> ids = new ArrayList<>();

        @Override
        protected List<String> doInBackground(Void... voids) {

            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection) WebServiceUrl.buildGetIds().openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                JSONResponseHandlerIds jsonResponseHandlerIds = new JSONResponseHandlerIds(ids);
                jsonResponseHandlerIds.readJsonStream(in);

            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                urlConnection.disconnect();
            }

            //


            return ids;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            Log.i("MuseumObjetcsIdsTask",""+strings.size());


            new FullMuseumObjetcsTask().execute(strings);

        }


    }
    // Tache en Arriere Plan tous les informations concernant item
    private class FullMuseumObjetcsTask extends AsyncTask<List<String>, Integer, List<MuseumObject>> {

        List<MuseumObject> MuseumObjects = new ArrayList<>();


        @Override
        protected List<MuseumObject> doInBackground(List<String>... strings) {



            for(String id : strings[0]){
                MuseumObject museumObject = new MuseumObject(id);
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) WebServiceUrl.buildGetMuseumObject(id).openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    JSONResponseHandlerMuseumObject jsonResponseHandlerMuseumObject = new JSONResponseHandlerMuseumObject(museumObject);
                    jsonResponseHandlerMuseumObject.readJsonStream(in);
                    MuseumObjects.add(museumObject);


                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    urlConnection.disconnect();
                }


            }
            return MuseumObjects;
        }

        @Override
        protected void onPostExecute(List<MuseumObject> museumObjects) {
            //Log.i("FullMuseumObjetcsTask",""+MuseumObjects.size());
            MainActivity.this.ListmuseumObjects = MuseumObjects;
            Log.i("ListmuseumObjects",""+ListmuseumObjects.size());

           // for(MuseumObject museumObject : MuseumObjects){
               // new GetImageTask(museumObject,adapter).execute();
            //}
            if(!museumDbHelper.getAllTeams().isEmpty()){
                museumDbHelper.deleteAllItems();
            }

            for(MuseumObject museumObject : MuseumObjects){
                MainActivity.this.museumDbHelper.addItem(museumObject);
            }

            MainActivity.this.adapter.notifyDataSetChanged();

            RecyclerViewTeams = findViewById(R.id.RecyclerView_MuseumObjects);
            RecyclerViewTeams.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            RecyclerViewTeams.addItemDecoration(new DividerItemDecoration(MainActivity.this, 0));

            MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, ListmuseumObjects,TriOption);

            RecyclerViewTeams.setAdapter(MainActivity.this.adapter);


        }
    }


    // fonction permet l'affichage le message //
    public void showDialog(final TextView txt_filter) {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Veuillez choisir un critère");

        //list of items
        final String[] items = {"id","name","categorie","description","timeFrame","year","brand","technicalDetails"};
        builder.setSingleChoiceItems(items, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // item selected logic

                        String strName = items[which];
                        txt_filter.setText(strName);
                        dialog.dismiss();
                    }
                });
        builder.setIcon(R.drawable.ic_filter_list_black_24dp);
           AlertDialog dialog = builder.create();
        // display dialog
         dialog.show();
    }

void getSearchedMuseumObject(String userInput,String critere){
        SearchList.clear();
        switch (critere){
            case "id":
                for(MuseumObject museumObject : ListmuseumObjects){
                    if(museumObject.getId().contains(userInput)){
                        SearchList.add(museumObject);
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);

                Log.i("SearchList","Sizee "+SearchList.size());
               break;
            case "name":
                for(MuseumObject museumObject : ListmuseumObjects){
                    if(museumObject.getName().contains(userInput)){
                        SearchList.add(museumObject);
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);

                Log.i("SearchList","Sizee "+SearchList.size());
                break;
            case "categorie":
                for(MuseumObject museumObject : ListmuseumObjects){
                    for(String categorie : museumObject.getCategories()){
                        if(categorie.contains(userInput)){
                            SearchList.add(museumObject);
                        }
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");
                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                Log.i("SearchList","Sizee "+SearchList.size());
                break;
            case "description":
                for(MuseumObject museumObject : ListmuseumObjects){
                    if(museumObject.getDescription().contains(userInput)){
                        SearchList.add(museumObject);
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);

                Log.i("SearchList","Sizee "+SearchList.size());
            case "timeFrame":
                for(MuseumObject museumObject : ListmuseumObjects){
                    for(int categorie : museumObject.getTimeFrame()){
                        if((""+categorie).contains(userInput)){
                            SearchList.add(museumObject);
                        }
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");
                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                Log.i("SearchList","Sizee "+SearchList.size());
                break;
            case "year":
                for(MuseumObject museumObject : ListmuseumObjects){
                    if((""+museumObject.getYear()).contains(userInput)){
                        SearchList.add(museumObject);
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
            case "brand":
                for(MuseumObject museumObject : ListmuseumObjects){
                    if(museumObject.getBrand().contains(userInput)){
                        SearchList.add(museumObject);
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");

                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);

                Log.i("SearchList","Sizee "+SearchList.size());
            case "technicalDetails":
                for(MuseumObject museumObject : ListmuseumObjects){
                    for(String categorie : museumObject.getTechnicalDetails()){
                        if(categorie.contains(userInput)){
                            SearchList.add(museumObject);
                        }
                    }
                }
                MainActivity.this.adapter = new RecyclerViewAdapter(MainActivity.this, SearchList,"NONE");
                RecyclerViewTeams.setAdapter(MainActivity.this.adapter);
                Log.i("SearchList","Sizee "+SearchList.size());
                break;
        }
    }

}
