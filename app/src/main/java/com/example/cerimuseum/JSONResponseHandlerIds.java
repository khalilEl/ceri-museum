package com.example.cerimuseum;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//analyse syntaxique des ids
public class JSONResponseHandlerIds {

    private static final String TAG = JSONResponseHandlerIds.class.getSimpleName();

    private List<String> museumObjectIds;

    List<String> ids;

    public JSONResponseHandlerIds(List<String> ids) {
        this.ids = ids;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMuseumObjectJson(reader);
        } finally {
            reader.close();
        }
    }



    private void readMuseumObjectJson(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            ids.add(reader.nextString());
        }
        reader.endArray();
    }
}
