package com.example.cerimuseum;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;


//Gestion des URLs de l'API du web
public class WebServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // /ids
    private static final String IDS = "ids";
    public static URL buildGetIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // /categories

    // /catalog

    // /items/<itemID>
    private static final String items = "items";
    public static URL buildGetMuseumObject(String itemID) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(items)
                .appendPath(itemID);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // /items/<itemID>/thumbnail

    private static final String THUMBNAIL = "thumbnail";

    public static URL buildThumbnail(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(items)
                .appendPath(idItem)
                .appendPath(THUMBNAIL);

        URL url = new URL(builder.build().toString());
        return url;
    }


    // items/<itemID>/images/<imageID>
    private static final String IMAGES = "images";

    public static URL buildImages(String idItem,String imageId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(items)
                .appendPath(idItem)
                .appendPath(IMAGES)
                .appendPath(imageId);

        URL url = new URL(builder.build().toString());
        return url;
    }

    //demos
    private static final String DEMOS = "demos";

    public static URL buildDemo() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMOS);

        URL url = new URL(builder.build().toString());
        return url;
    }

}
